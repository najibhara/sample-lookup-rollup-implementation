# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a sample implementation of Rollup Summary fields when the possibility of using the standard feature is not available.

This solution addresses the following limitations:

* [Only 10 rollup summary fields allowed per object on master detail relationships](http://ap1.salesforce.com/help/doc/en/limits.htm#CustomFieldLimitDetails)

* Rollup child sobject records part of a lookup relationship. Native rollup summary fields are not available on LOOKUP relationships.

This sample implementation features two objects (Contact & Expense) linked with a Lookup relationship.

Abhinav Gupta made a nice implementation of a library that solves the problem. This sample is based on Abhinav's work: [https://github.com/abhinavguptas/Salesforce-Lookup-Rollup-Summaries](https://github.com/abhinavguptas/Salesforce-Lookup-Rollup-Summaries) 

Future versions will aim at offering a declarative way of extending the standard Rollup feature, like what Andrew Fawcett did: [https://github.com/afawcett/declarative-lookup-rollup-summaries/](https://github.com/afawcett/declarative-lookup-rollup-summaries/)


### How do I get set up? ###
In order to test this sample application, just deploy the metadata within the src folder to your dev org.
Next, you will need to schedule the Apex class ContactRollupScheduler.