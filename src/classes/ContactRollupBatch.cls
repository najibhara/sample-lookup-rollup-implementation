global class ContactRollupBatch implements Database.Batchable<sObject> {
	
	String query;
	
	global ContactRollupBatch() {
		query = 'Select Id, Total_Expenses_for_the_Last_12_Months__c From Contact';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		ContactRollupCalculator.IContactRollupCalculator calculator = (ContactRollupCalculator.IContactRollupCalculator) ObjectFactory.GetSingletonInstance('ContactRollupCalculator');
		calculator.calculateFromBatch(scope);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}