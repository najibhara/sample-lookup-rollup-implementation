@isTest
private class ContactRollupBatchTest {
	
	private static void testSetup()
	{
		ObjectFactory.MockSingletonInstance('ContactRollupCalculator', new ContactRollupCalculatorMock());
	}

	@isTest static void itShould_callCalculator() {
		// Given a master record : contact
		Contact contact = new Contact(lastname='TestContact', Total_Expenses_for_the_Last_12_Months__c=0);
		insert contact;
		Contact retrievedContact = [Select Id, Total_Expenses_for_the_Last_12_Months__c From Contact Where Id =:contact.Id][0];
		List<SObject> expectedScope = new List<SObject>{retrievedContact};

		//and given the mock setup
		testSetup();

		Test.startTest();

		//when you launch the batch
		ContactRollupBatch batch = new ContactRollupBatch();
		Id batchId = Database.executeBatch(batch);

		Test.stopTest();

		//it should call the calculator with the right method
		System.assert(TestMockUtilities.Tracer.HasBeenCalled('ContactRollupCalculatorMock', 'calculateFromBatch'));

		//with the right parameters
		
		System.assert(TestMockUtilities.Tracer.HasBeenCalledWithParameter('ContactRollupCalculatorMock', 'calculateFromBatch', 'scope', expectedScope));
	}	
}