public class ContactRollupCalculator implements IClass{

	public Object getInstance()
	{
		return new Impl();
	}

	public interface IContactRollupCalculator
	{
		void calculateFromTrigger(TriggerParameters tp);
		void calculateFromBatch(List<sObject> scope);
		void defineContactRollup();
		List<Expense__c> filterOnUpdate(Map<Id, Sobject> oldMap, Map<Id, Sobject> newMap);
		Boolean hasAmountChanged(Expense__c oldVersion, Expense__c newVersion);
	}

	private class Impl implements IContactRollupCalculator
	{

		private LREngine.Context ctx;
		private Set<Id> masterRecordIds;
		private Sobject[] masters;

		public Impl() {
			masters = new List<sObject>();
		}

		public void calculateFromTrigger(TriggerParameters tp)
		{
			System.debug('ContactRollupCalculator.calculateFromTrigger Executing');
			System.debug('With the following Trigger Parameters : '+tp);

			List<Expense__c> listExpenses = new List<Expense__c>();

			if (tp.tEvent == TriggerParameters.TriggerEvent.afterUpdate)
			{
				System.debug('ContactRollupCalculator.calculateFromTrigger afterUpdate Event Executing');
				listExpenses = filterOnUpdate(tp.oldMap, tp.newMap);
			}
			else if (tp.tEvent == TriggerParameters.TriggerEvent.afterDelete)
			{
				System.debug('ContactRollupCalculator.calculateFromTrigger afterDelete Event Executing');
				listExpenses = (List<Expense__c>) tp.oldList;
			}
			else
			{
				System.debug('ContactRollupCalculator.calculateFromTrigger afterInsert or AfterUndelete Events Executing');
				listExpenses = (List<Expense__c>) tp.newList;
			}

			defineContactRollup();

			masters = LREngine.rollUp(ctx, listExpenses);

			try {
	    		update masters;
			} catch(DmlException e) {
	    		System.debug('The following exception has occurred: ' + e.getMessage());
			}
		}

		public void calculateFromBatch(List<sObject> scope)
		{
			List<Contact> contacts = (List<Contact>) scope;

			Set<Id> masterRecordIds = new Set<Id>();

			for (Contact contact : contacts)
			{
				masterRecordIds.add(contact.Id);
			}

			defineContactRollup();

			masters = LREngine.rollUp(ctx, masterRecordIds);

			try {
	    		update masters;
			} catch(DmlException e) {
	    		System.debug('The following exception has occurred: ' + e.getMessage());
			}
		}

		public void defineContactRollup()
		{
			ctx = new LREngine.Context(Contact.SobjectType, 
										Expense__c.SobjectType,
										Schema.SObjectType.Expense__c.fields.Contact__c,
										UtilityClass.getRollupFilterCondition()
									);

			ctx.add(new LREngine.RollupSummaryField(Schema.SObjectType.Contact.fields.Total_Expenses_for_the_Last_12_Months__c, 
													Schema.SObjectType.Expense__c.fields.Amount__c,
													LREngine.RollupOperation.Sum
												));
		}

		public List<Expense__c> filterOnUpdate(Map<Id, Sobject> oldMap, Map<Id, Sobject> newMap)
		{
			List<Expense__c> filteredExpenses = new List<Expense__c>();

			for (Id expenseId : newMap.keySet())
			{
				Expense__c oldVersion = (Expense__c) oldMap.get(expenseId);
				Expense__c newVersion = (Expense__c) newMap.get(expenseId);

				if (hasAmountChanged(oldVersion, newVersion))
					filteredExpenses.add(newVersion);
			}

			return filteredExpenses;
		}

		public Boolean hasAmountChanged(Expense__c oldVersion, Expense__c newVersion)
		{
			return (oldVersion.Amount__c != newVersion.Amount__c);
		}
	}
}