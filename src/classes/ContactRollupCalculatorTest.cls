@isTest
private class ContactRollupCalculatorTest {

	@isTest static void itShould_calculateFromBatch() {
		//given a master record : contact
		Contact masterRecord = new Contact(lastname='test');
		insert masterRecord;

		//and related children records : expenses
		Expense__c expense1 = new Expense__c(Contact__c=masterRecord.Id, Custom_Created_Date__c=Date.Today(), Amount__c=100);
		Expense__c expense2 = new Expense__c(Contact__c=masterRecord.Id, Custom_Created_Date__c=Date.Today(), Amount__c=200);

		insert new List<Expense__c>{expense1, expense2};

		Test.startTest();
		//when the calculation from batch is called
		ContactRollupCalculator.IContactRollupCalculator calculator = (ContactRollupCalculator.IContactRollupCalculator) ObjectFactory.GetSingletonInstance('ContactRollupCalculator');
		calculator.calculateFromBatch(new List<SObject>{masterRecord});

		Test.stopTest();
		//it should update the master record
		Contact retrievedRecord = [Select Id, Total_Expenses_for_the_Last_12_Months__c From Contact Where Id =: masterRecord.Id][0];

		System.assertEquals(300, retrievedRecord.Total_Expenses_for_the_Last_12_Months__c);
	}

	@isTest static void itShould_filterOnUpdate_positive() {

		// given the old and new versions of a list of SObjects
		Expense__c expense1 = new Expense__c(Id=TestUtility.getFakeId(Expense__c.SObjectType), Amount__c=100);
		Expense__c expense2 = new Expense__c(Id=TestUtility.getFakeId(Expense__c.SObjectType), Amount__c=200);

		Map<Id, Sobject> oldMap = new Map<Id, Sobject>();
		oldMap.put(expense1.Id, expense1);
		oldMap.put(expense2.Id, expense2);

		Expense__c expense3 = expense1.clone(true, true, false, false);
		expense3.Amount__c = 200;
		Expense__c expense4 = expense2.clone(true, true, false, false);
		expense4.Amount__c = 400;
		
		Map<Id, Sobject> newMap = new Map<Id, Sobject>();
		newMap.put(expense3.Id, expense3);
		newMap.put(expense4.Id, expense4);

		List<Expense__c> expenses = new List<Expense__c>{expense3, expense4};

		Test.startTest();
		//when filterOnUpdate is called
		ContactRollupCalculator.IContactRollupCalculator calculator = (ContactRollupCalculator.IContactRollupCalculator) ObjectFactory.GetSingletonInstance('ContactRollupCalculator');
		List<Expense__c> result = calculator.filterOnUpdate(oldMap, newMap);


		Test.stopTest();

		//it should return a correct list
		System.assert(!(result.isEmpty()));
		System.assertEquals(expenses, result);
	}

	@isTest static void itShould_filterOnUpdate_negative() {

		// given the old and new versions of a list of SObjects
		Expense__c expense1 = new Expense__c(Id=TestUtility.getFakeId(Expense__c.SObjectType), Amount__c=100);
		Expense__c expense2 = new Expense__c(Id=TestUtility.getFakeId(Expense__c.SObjectType), Amount__c=200);

		List<Expense__c> expenses = new List<Expense__c>{expense1, expense2};

		Map<Id, Sobject> oldMap = new Map<Id, Sobject>();
		oldMap.put(expense1.Id, (Expense__c) expense1);
		oldMap.put(expense2.Id, (Expense__c) expense2);
		
		Map<Id, Sobject> newMap = new Map<Id, Sobject>();
		newMap.put(expense1.Id, (Expense__c) expense1);
		newMap.put(expense2.Id, (Expense__c) expense2);

		Test.startTest();
		//when filterOnUpdate is called
		ContactRollupCalculator.IContactRollupCalculator calculator = (ContactRollupCalculator.IContactRollupCalculator) ObjectFactory.GetSingletonInstance('ContactRollupCalculator');
		List<Expense__c> result = calculator.filterOnUpdate(oldMap, newMap);


		Test.stopTest();

		//it should call the hasAmountChanged Method
		//System.assert(TestMockUtilities.Tracer.HasBeenCalled('ContactRollupCalculatorMock', 'hasAmountChanged'));

		//it should return an empty list
		System.assert(result.isEmpty());
	}

	@isTest static void itShould_hasAmountChanged_positive() {
		//given two versions of the expense having a different amount
		Expense__c oldVersion = new Expense__c(Amount__c=100);
		Expense__c newVersion = new Expense__c(Amount__c=200);

		// and a ContactRollupCalculator
		ContactRollupCalculator.IContactRollupCalculator calculator = (ContactRollupCalculator.IContactRollupCalculator) ObjectFactory.GetSingletonInstance('ContactRollupCalculator');

		//when hasAmountChanged is called
		Boolean result = calculator.hasAmountChanged(oldVersion, newVersion);

		//it should state that the amount has changed
		System.assertEquals(true, result, 'result should be true');
	}
	
	@isTest static void itShould_hasAmountChanged_negative() {
		//given two versions of the expense having the same amount
		Expense__c oldVersion = new Expense__c(Amount__c=100);
		Expense__c newVersion = new Expense__c(Amount__c=100);

		// and a ContactRollupCalculator
		ContactRollupCalculator.IContactRollupCalculator calculator = (ContactRollupCalculator.IContactRollupCalculator) ObjectFactory.GetSingletonInstance('ContactRollupCalculator');

		//when hasAmountChanged is called
		Boolean result = calculator.hasAmountChanged(oldVersion, newVersion);

		//it should state that the amount has changed
		System.assertEquals(false, result, 'result should be false');
	}

	@isTest static void coverTriggerscenarios() {
		//given a master record : contact
		Contact masterRecord = new Contact(lastname='test');
		insert masterRecord;

		//and related children records : expenses
		Expense__c expense1 = new Expense__c(Contact__c=masterRecord.Id, Custom_Created_Date__c=Date.Today(), Amount__c=100);
		Expense__c expense2 = new Expense__c(Contact__c=masterRecord.Id, Custom_Created_Date__c=Date.Today(), Amount__c=200);

		insert new List<Expense__c>{expense1, expense2};

		expense1.Amount__c = 400;
		update expense1;

		delete expense2;

		undelete expense2;
	}
}