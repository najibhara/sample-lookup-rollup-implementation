global class ContactRollupScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {

		ContactRollupBatch batch = new ContactRollupBatch();
		Database.executebatch(batch);
	}
}