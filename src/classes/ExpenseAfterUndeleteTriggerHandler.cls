public class ExpenseAfterUndeleteTriggerHandler extends TriggerHandlerBase {
	public override void mainEntry(TriggerParameters tp) {
		System.debug('ExpenseAfterUndeleteTriggerHandler.mainEntry Executing');
		process(tp);
	}

	private static void process(TriggerParameters tp)
	{
		System.debug('ExpenseAfterUndeleteTriggerHandler.process Executing');
		ContactRollupCalculator.IContactRollupCalculator calculator = (ContactRollupCalculator.IContactRollupCalculator) ObjectFactory.GetSingletonInstance('ContactRollupCalculator');
		calculator.calculateFromTrigger(tp);
	}

	public override void inProgressEntry(TriggerParameters tp) {
		System.debug('This is an example for reentrant code...');
	}
}