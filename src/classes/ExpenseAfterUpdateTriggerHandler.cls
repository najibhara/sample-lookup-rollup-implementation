public class ExpenseAfterUpdateTriggerHandler extends TriggerHandlerBase {
	public override void mainEntry(TriggerParameters tp) {
		process(tp);
	}

	private static void process(TriggerParameters tp)
	{
		ContactRollupCalculator.IContactRollupCalculator calculator = (ContactRollupCalculator.IContactRollupCalculator) ObjectFactory.GetSingletonInstance('ContactRollupCalculator');
		calculator.calculateFromTrigger(tp);
	}

	public override void inProgressEntry(TriggerParameters tp) {
		System.debug('This is an example for reentrant code...');
	}
}