public interface IClass {

	//Define the method signature to be implemented in classes that implements the interface
	Object getInstance();
}