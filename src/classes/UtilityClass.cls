/**
* @author Hari Krishnan
* @date 07/16/2013
* @description This class has helper methods.
*/

public with sharing class UtilityClass {
	
	private static String DATESUFFIX = 'T00:00:00Z';

	/**
	* @author Hari Krishnan
	* @date 07/16/2013
	* @description Gets the type name of the SObject.
	* @param SObject The SObject for which the name to be obtained.
	* @return String - The type name.
	*/
	public static String getSObjectTypeName(SObject so) {
		return so.getSObjectType().getDescribe().getName();
	}

	private static String getLastTwelveMonthsDate() {
		Date lastTwelveMonths = Date.newInstance(Date.today().year() -1, Date.today().month(), Date.today().day());
		return String.valueOf(lastTwelveMonths);
	}

	private static String getRollupFieldFilter() {
		return 'Custom_Created_Date__c > ';
	}

	public static String getRollupFilterCondition() {
		return getRollupFieldFilter() + getLastTwelveMonthsDate() + DATESUFFIX;
	} 
}