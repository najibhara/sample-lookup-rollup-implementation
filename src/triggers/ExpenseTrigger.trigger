trigger ExpenseTrigger on Expense__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	// This is the only line of code that is required.
	TriggerFactory.createTriggerDispatcher(Expense__c.sObjectType);
}